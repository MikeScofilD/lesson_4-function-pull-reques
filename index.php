<?php
include 'mathFunctions/distance.php';
declare(ticks=1);

//Задача1 (для каждой подзадачи нужно использовать уже встроенную в php функцию, для остальных - написать свою):
//1)Округлите число, записанное в переменную $number, до двух знаков после запятой и выведите результат на экран;
$chislo = 312.3123;
echo round($chislo,2).'</br>';
//2)Дана строка 'php', сделайте из нее строку 'PHP';
$php = 'php';
$PHP = strtoupper($php);
echo $PHP.'</br>';


//3)Дана строка 'PHP', сделайте из нее строку 'php';
echo strtolower($PHP).'</br>';

//4)Напишите функцию, которая принимает строку с названиями 4 овощей для салата через комму,
// а возвращает массивы из 4 элементов, вызовите функцию и поместите все 4 элемента в отдельную переменную в одно действие;
$ingr = 'Капуста Горошек Огурец Укроп Соль';
$arr = [];
$arr = explode(" ",$ingr);
echo "<pre>";
echo print_r($arr);
echo "</pre>";

//5)Дана строка '31.12.2013', замените все точки на дефисы;
$date = '31.12.2013';
$date = str_replace('.','-',$date);
echo $date.'</br>';


//6)Есть переменная $password,
// в которой хранится пароль пользователя,
// напишите функцию, которая выведите пользователю сообщение о том,
// что если количество символов пароля больше 5-ти и меньше 10-ти,
// то пароль подходит, иначе сообщение о том, что нужно придумать другой пароль;
$password = '123312';
echo "Длина пароля ".strlen($password)."</br>";
if (strlen($password)>=5 && strlen($password)<10){
    echo "Пароль подходит</br>";
} else {
    echo "Нужно придумать другой пароль</br>";
}

//7)дана строка '1234567890', разбейте ее на массив с элементами по 2 цифры в каждом;
$str = '1234567890';
$arr = str_split($str);
echo '<pre>';
print_r(array_chunk($arr,2));
echo '</pre>';

//8)сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на русском языке.
$day = 5;
function Day_of_Week($day) {
    switch ($day) {
        case 1:
            echo "Понедельник</br>";
            break;
        case 2:
            echo "Вторник</br>";
            break;
        case 3:
            echo "Среда</br>";
            break;
        case 4:
            echo "Четверг</br>";
            break;
        case 5:
            echo "Пятница</br>";
            break;
        case 6:
            echo "Суббота</br>";
            break;
        case 7:
            echo "Воскресенье</br>";
            break;
        default:
            echo "Неправельный день недели";
    }
}
Day_of_Week($day);

//9)дан массив 'a'=>1, 'b'=>2, 'c'=>3, поменяйте в нем местами ключи и значения;
$arr = [
    'a'=>1,
    'b'=>2,
    'c'=>3
];
echo '<pre>';
print_r($arr);
echo '</pre>';

$arr = array_flip($arr);
echo '<pre>';
print_r($arr);
echo '</pre>';


//10)найдите все счастливые билеты, счастливый билет - это билет,
// в котором сумма первых трех цифр его номера равна сумме вторых трех цифр его номера,
// функция должна принимать номер билета и возвращать true или false;
$ticket = [1,2,3,3,2,1];
$t1 = array_sum(array_slice($ticket,0, 3));
$t2 = array_sum(array_slice($ticket, 3));
if ($t1==$t2) {
    echo "Поздравляем ваш белет счастливый</br>";
} else {
    echo "Возможно вам повезет в другой раз</br>";
}

//11)сделайте функцию, которая отнимает от первого числа второе и делит на третье;
function t11($num1 = 15,$num2 = 2,$num3 = 3): float {
    return ($num1-$num2)/$num3;
}
echo t11()."</br>";


//12)дан массив с элементами 1, 2, 3, 4, 5, с помощью функции array_slice
// создайте из него массив $result с элементами 2, 3, 4;
$arr = [1,2,3,4,5];
$result = array_slice($arr,1,3);

echo "<pre>";
print_r($result);
echo "</pre>";


//13)есть строка, проверьте через функцию,
// что она начинается на 'http://' или на 'https://', если это так, выведите 'да', если не так - 'нет';
$url = 'https://itea.ua/';
//$url2 = 'http://itea.ua/';
$https = substr($url,0,8);
$http = substr($url,0,7);
echo "https=".$https."</br>";
echo "http=".$http."</br>";
if ($https == 'https://'||$http=='http://') {
    echo true;
}
else{
    echo false;
}
echo "</br>";
//14)дана строка $str,
// замените в ней все буквы 'a' на цифру 1, буквы 'b' - на 2,
// а буквы 'c' - на 3,
// решите задачу двумя способами работы с функцией strtr (массив замен и две строки замен);
function generateRandomString($length = 20): string {
    $characters = 'abcdefghijklmnopqrstuvwxyz';
//ABCDEFGHIJKLMNOPQRSTUVWXYZ
//0123456789
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
$str = generateRandomString();
//$str = 'abababdabdbsdcc';
echo $str."</br>";
$newStr = strtr($str, ['a'=>1, 'b'=>'2', 'c'=>'3']);
echo $newStr."</br>";

//15)сделайте функцию getFormattedDate,
// которая принимает на вход три параметра: день, месяц и год рождения,
// а возвращает их строкой в отформатированном виде, например: 30:02:1953,
// день и месяц нужно форматировать так, чтобы при необходимости добавлялся 0 слева.
// (например, если в качестве месяца пришла цифра 7,
// то в выходной строке она должна быть представлена как 07), подсказка - sprintf;
function getFormattedDate($day,$month,$year): string {
    if($day>0&&$day<10){
        $day = sprintf("%02d",$day);
    } else if($day>31||$day<=0){
        return 'Некоректное значение дня</br>';
    }
    if($month>0&&$month<10){
        $month = sprintf("%02d",$month);
    } else if ($month>12||$month<=0) {
        return 'Некоректное значение месяца</br>';
    }
    if ($year<1950||$year>2020) {
        return 'Некоректное значение года</br>';
    }
    return $day.':'.$month.':'.$year.'</br>';
}

echo getFormattedDate(7,7,2019);

//16)напишите функцию (название придумайте сами),
// которая принимает два года рождения и возвращает строку с разницей в возрасте в виде The age difference is 11;
function ageDifference($y1=1990,$y2=2000): int
{
    if ($y1>$y2) {
        return $y1-$y2;
    } else{
        return $y2-$y1;
    }

}
echo 'The age difference is '.ageDifference(1990,2008)."</br>";

//17)реализуйте функцию, которая округляет возраст так, что половина округляется в нижнюю сторону.
// То есть если человеку десять с половиной лет, то функция должна вернуть 10.
// Если ему хотя бы немного больше десяти с половиной, то округление идет в верхнюю сторону;
function myRound($age){
    echo "Age = ".round($age,0,PHP_ROUND_HALF_DOWN)."</br>";
}
myRound(13.6);
myRound(13.3);
myRound(15.3);
myRound(15.6);
myRound(13.5);

//18)даны числа 4, -2, 5, 19, -130, 0, 10.
// Напишите функцию, которая вернет минимальное и максимальное число и запишите результат в 2 переменных за одну манипуляцию;
$number = [4, -2, 5, 19, -130, 0, 10];
function min_max($number): string {
    $min = min($number);
    $max = max($number);
    return "Min: ".$min." Max: ".$max."</br>";
}
$result = min_max($number);
echo $result;

//19)В задачах выше, используйте все возможные варианты строгой типизации.

//Задача2:
//1)Один ученый любит составлять карты, и ему часто нужно выводить на экран повторяющиеся символы для визуализации маршрутов.
//2)Например, так он иллюстрирует грунтовые дороги между городами: Нижние Варты =-=-=-=- Myr
//3)А так иллюстрирует магистрали: Киев ======== Чоп
//4)В документации PHP ученый нашёл функцию str_repeat(), которая возвращает повторяющуюся строку;
//5)Использую эту функцию, напишите свою, которая принимает 4 аргумента:
// 2 строки с названиями городов, количество повторений разделителя и сам разделитель,
// как не обязательный аргумент (по умолчанию ‘=’), который должен дублироваться;
//6)Цель функции - соединить 2 города с помощью дороги (повторяющегося) разделителя в одну строку;
//7)Используйте все возможные варианты строгой типизации.
function way($town1,$town2,$repeat,$separator='=') : string {

//    if ($town1 == 'Киев'&&$town2=='Чоп'){
//        $result = str_repeat('=', $repeat);
//    } else if ($town1 == 'Варты'&&$town2=='Myr'){
//        $result = str_repeat('=-', $repeat);
//    }
    $result = str_repeat($separator, $repeat);
    return $town1." ".$result." ".$town2."</br>";
}
echo way('Киев','Чоп',4);

//Задача3:
//1)создайте папку mathFunctions в корне проекта,
// в нем создайте php-файл, в котором будут находится функции для работы с дистанцией,
// название файла придумайте сами, которое будет отвечать содержимому;
//2)в этом файле напишите функцию,
// которая будет считать дистанцию в километрах между двумя точками
// (2 числа передаваемые в аргументе) и возвращать эту дистанцию;
//3)используйте все возможные варианты строгой типизации;
//4)подключите данный файл в index.php и выведите на экран результат выполнения функции для 3 разных пар точек;
echo round(distance(20,20,30,30),2);
echo "</br>";
//Задача4:
//1)в папке mathFunctions в корне проекта, создайте php-файл,
// в котором будут находится функции для работы с массивами,
// название файла придумайте сами, которое будет отвечать содержимому;
//2)в этом файле напишите функцию, которая будет считать количество четных индексов в переданном массиве и возвращать это число;
//3)используйте все возможные варианты строгой типизации;
//4)в этом файле используйте namespace;
//5(подключите данный файл в index.php и выведите на экран результат выполнения функции для 2 разных массивов, используя namespace;
require 'mathFunctions/even.php';
$even = new mathFunctions\Even();

echo "<pre>";
echo "Even ";print_r($even->Even());
echo "</pre>";

$arr = [35,-10,12,312,1,3,4,-112,3,21,-1231,-321,123,4234,-12,12414,-2];
$even2 = new mathFunctions\Even($arr);

echo "<pre>";
echo "Even2 ";print_r($even2->Even());
echo "</pre>";

//Задача5:
//Есть файл - https://docs.google.com/spreadsheets/d/1bUlxpy2o6kKZO82QQeKy7tgw0qlV1nHd217zYMhN21A/edit?usp=sharing
// - в нем список функций, которые нужно использовать в дз;
//у каждого студента по 5 функций + 3 функции обязательные для всех;
//ваша задача написать пример (а лучше парочку) для каждой вашей функции, чтобы показать ее работу;
//все ваши функции и примеры напишите в отдельный файл с названием myFunctions.php и определите в отдельный неймспейс;
//5)Пример: у меня попалась функция coun(),
// я создаю несколько разных массивов,
// используя эту функцию показываю,
// как она работает - вывожу на экран сам массив и к-тво элементов в нем, подсчитанное этой функцией;
//6)каждый сделает свою часть и посмотрит эту же задачу у остальных,
// таким образом все узнают, как работает та или иная функция;
//7)просьба отнестись ответственно к этой задаче ибо от вашего выполнения зависит понимание остальных студентов.

require 'mathFunctions/myFunctions.php';
$even = new mathFunctions\myFunctions();
$even->myFunctions();


//Задача6 (на рекурсию):
//дано какое-то число;
//сложите его цифры;
//если сумма получилась более 9-ти, опять сложите его цифры,
//и так, пока сумма не станет однозначным числом (9 и менее).

//function digit($num)
//{
//    echo "1) Получаем Num из функции = ".$num."</br>";
//    $result = array_sum(str_split($num , 1));
//    echo "2) Получаем Result от array_sum = ".$result."</br>";
//    $num = $result;
//    echo "3) Num = ".$num." Result= ".$result."</br>";
//    if ($num > 9) {
//        echo "4) Проверка if ".$num."</br>";
//        digit($num);
//        echo "5) if  Return num = ".$num."</br>";
//        return $num;
//    }
//    else{
//        echo "5) else  Return num = ".$num."</br>";
//        return $num;
//    }
//}
function digit($num): int
{
    $result = array_sum(str_split($num , 1));
    $n  =  $result;
    if ($result > 9) {
        return digit($n);
    }
    return $n;
}
echo "Digit = ".digit(998);